<?php

class jqConsumer extends jqGrid
{
    
    const CONSUMER_TBL         = 'CONSUMER_TBL';
    const GROUP_TBL            = 'GROUP_TBL';
    
    const ID               = 'ID';
    const ID_GROUP         = 'ID_GROUP';
    const LOGIN            = 'LOGIN';
    const PASSWORD         = 'PASSWORD';
    const EMAIL            = 'EMAIL';
    const ACCOUNT_EXPIRED  = 'ACCOUNT_EXPIRED';
    const AVATAR_EXTENSION = 'AVATAR_EXTENSION';
    
    const NAME             = 'NAME';
    
    const FORM_EDIT_WIDTH  = 400;
    const GRID_WIDTH       = 1000;

    protected function init()
    {
        $this->table = self::CONSUMER_TBL;

        $this->options = array(
            'caption' => 'Список абонентов',
            'width'   => self::GRID_WIDTH,
        );

        $this->query = "
            SELECT {fields}
            FROM {$this->table} 
            WHERE {where}
        ";
            
        $this->file_ext = array('jpg' => 'jpg', 'gif' => 'gif', 'png' => 'png');
        $this->img_path = 'upload/' . $this->grid_id . DIRECTORY_SEPARATOR;
        //$selectGroup = $this->_getReplace(self::GROUP_TBL, self::NAME);
        
        # Set columns
        $this->cols = array(
            self::ID               => array(
                'label' => 'код абонента',
                'width' => 40,
                'align' => 'center',
            ),
            self::ID_GROUP         => array(
                'label' => 'код группы',
                'width' => 40,
                'align' => 'center',
                'editable'    => true,
                //'edittype'    => 'select',
                //'editoptions' => array(
                 //   'value' => new jqGrid_Data_Value($selectGroup),
                //),
            ),
            self::LOGIN            => array(
                'label' => 'логин абонента',
                'width' => 50,
                'align' => 'center',
                'editable'=> true,
            ),
            self::PASSWORD         => array(
                'label' => 'пароль абонента',
                'width' => 50,
                'align' => 'center',
                'editable'=> true,
            ),
            self::EMAIL            => array(
                'label' => 'адрес электронной почты',
                'width' => 50,
                'align' => 'center',
                'editable'=> true,
            ),
            self::ACCOUNT_EXPIRED  => array(
                'label' => 'действителен',
                'width' => 50,
                'align' => 'center',
                'editable'      => true,
                'searchoptions' => array('dataInit' => $this->initDatepicker(array(
                    'dateFormat' => 'yy-mm-dd',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'minDate' => '1950-01-01',
                    'maxDate' => '2000-01-01',
                    'onSelect' => new jqGrid_Data_Raw('function(){$grid[0].triggerToolbar();}'),
                ))),
                'search_op' => 'equal',

            ),
            'image' => array(
                'label' => 'Image',
                'width' => 14,
                'manual' => true, //manually create images in PHP
                //you can use JS formatter instead
                'encode' => false,
                'sortable' => false,
            ),
            'upload' => array('label' => 'Upload image',
                'manual' => true,
                'hidden' => true,
                'editable' => true,
                'edittype' => 'file',
                'editrules' => array('edithidden' => true),
                'formoptions' => array('elmsuffix' => '&nbsp;&nbsp;&nbsp;' . implode(', ', $this->file_ext)),
            ),
        );

        # Set nav
        $this->nav = array(
            'add'     => true,
            'edit'    => true,
            'del'     => true,
            'prmEdit' => array(
                'width' => self::FORM_EDIT_WIDTH,
            ),
            'prmAdd'  => array(
                'width' => self::FORM_EDIT_WIDTH,
            ),
        );

        # Add filter toolbar
        $this->render_filter_toolbar = true;

        $this->_scriptBefore[] = <<<JS

    var opts = {
        
        ondblClickRow: function(id)
        {
            $(this).editGridRow(id, { "width": "{$this->nav['prmEdit']['width']}" });
        },

    };
JS;
    }

    protected function searchOpDateRange($c, $val)
    {
        //--------------
        // Date range
        //--------------

        if(strpos($val, ' - ') !== false)
        {
            list($start, $end) = explode(' - ', $val, 2);

            $start = strtotime(trim($start));
            $end = strtotime(trim($end));

            if(!$start or !$end)
            {
                throw new jqGrid_Exception('Invalid date format');
            }

            #Stap dates if start is bigger than end
            if($start > $end)
            {
                list($start, $end) = array($end, $start);
            }

            $start = date('Y-m-d', $start);
            $end = date('Y-m-d', $end);

            return $c['db'] . " BETWEEN '$start' AND '$end'";
        }

        //------------
        // Single date
        //------------

        $val = strtotime(trim($val));

        if(!$val)
        {
            throw new jqGrid_Exception('Invalid date format');
        }

        $val = date('Y-m-d', $val);

        return "DATE({$c['db']}) = '$val'";
    }

    protected function initDatepicker($options = null)
    {
        $options = is_array($options) ? $options : array();

        return new jqGrid_Data_Raw('function(el){$(el).datepicker(' . jqGrid_Utils::jsonEncode($options) . ');}');
    }

    protected function initDateRangePicker($options = null)
    {
        $options = is_array($options) ? $options : array();

        return new jqGrid_Data_Raw('function(el){$(el).daterangepicker(' . jqGrid_Utils::jsonEncode($options) . ');}');
    }
    #Init file processing
    protected function operData($r)
    {
        if(isset($_FILES['upload']))
        {
            require_once 'misc/upload.class.php';

            $this->upload = new upload($_FILES['upload']);

            if(!$this->upload->uploaded)
            {
                throw new jqGrid_Exception('Upload failed');
            }

            if(!in_array($this->upload->file_src_name_ext, $this->file_ext))
            {
                throw new jqGrid_Exception('Bad file type');
            }

            $r['filename'] = $this->upload->file_src_name;
            $r['size'] = $this->upload->file_src_size;
        }

        return $r;
    }
    #Upload
    protected function operAfterAddEdit($id)
    {
        if(isset($_FILES['upload']))
        {
            ini_set('memory_limit', '128M');

            $this->upload->file_new_name_body = $id;
            $this->upload->file_auto_rename = false;
            $this->upload->file_overwrite = true;
            $this->upload->image_resize = true;
            $this->upload->image_x = 75;
            $this->upload->image_y = 75;
            $this->upload->image_ratio_crop = true;

            $this->upload->process($this->img_path);
        }
    }

//    protected function opAdd($ins)
//    {
//        if (empty($ins[self::ACTION_CODE])) {
//            throw new jqGrid_Exception('Поле "' . self::LABEL_ACTION_CODE . '" не должно быть пустым');
//        }
//        $ins[self::ACTION_CODE] = trim($ins[self::ACTION_CODE]);
//        if (mb_strlen($ins[self::ACTION_CODE], 'utf-8') > self::ACTION_CODE_LENGTH) {
//            throw new jqGrid_Exception('Максимально допустимая длина поля "' . self::LABEL_ACTION_CODE . '" ' . self::ACTION_CODE_LENGTH . ' символов');
//        }
//        if ($this->_getIdByCode($ins[self::ACTION_CODE], self::ACTION_CODE, self::USERS_ACTION)) {
//            throw new jqGrid_Exception('Код действия уже используется');
//        }
//        if (!$this->_checkActionCode($ins[self::ACTION_CODE])) {
//            throw new jqGrid_Exception('Поле "' . self::LABEL_ACTION_CODE . '" должно начинаться с латинской буквы и может содержать нижнее подчеркивание и числа.');
//        }
//
//        $ins[self::ACTION_DESCR] = trim($ins[self::ACTION_DESCR]);
//        if (mb_strlen($ins[self::ACTION_DESCR], 'utf-8') > self::ACTION_DESCR_LENGTH) {
//            throw new jqGrid_Exception('Максимально допустимая длина поля "' . self::LABEL_ACTION_DESCR . '" ' . self::ACTION_DESCR_LENGTH . ' символов');
//        }
//
//        parent::opAdd($ins);
//    }
//
//    protected function opEdit($id, $upd)
//    {
//        if (empty($upd[self::ACTION_CODE])) {
//            throw new jqGrid_Exception('Поле "' . self::LABEL_ACTION_CODE . '" не должно быть пустым');
//        }
//        $upd[self::ACTION_CODE] = trim($upd[self::ACTION_CODE]);
//        if (mb_strlen($upd[self::ACTION_CODE], 'utf-8') > self::ACTION_CODE_LENGTH) {
//            throw new jqGrid_Exception('Максимально допустимая длина поля "' . self::LABEL_ACTION_CODE . '" ' . self::ACTION_CODE_LENGTH . ' символов');
//        }
//         $resCode = $this->_getIdByCode($upd[self::ACTION_CODE], self::ACTION_CODE, self::USERS_ACTION);
//         if ($resCode) {
//            if ($resCode <> $id) {
//                throw new jqGrid_Exception('Код действия уже используется');
//            }
//        }
//        if (!$this->_checkActionCode($upd[self::ACTION_CODE])) {
//            throw new jqGrid_Exception('Поле "' . self::LABEL_ACTION_CODE . '" должно начинаться с латинской буквы и может содержать нижнее подчеркивание и числа.');
//        }
//
//        $upd[self::ACTION_DESCR] = trim($upd[self::ACTION_DESCR]);
//        if (mb_strlen($upd[self::ACTION_DESCR], 'utf-8') > self::ACTION_DESCR_LENGTH) {
//            throw new jqGrid_Exception('Максимально допустимая длина поля "' . self::LABEL_ACTION_DESCR . '" ' . self::ACTION_DESCR_LENGTH . ' символов');
//        }
//
//        parent::opEdit($id, $upd);
//    }

    /**
     * Проверяет на наличие латинских символов, знака подчеркивания и цифр 
     * 
     * @param string $string
     * @return boolean
     */
//    protected function _checkActionCode($string)
//    {
//        $pattern = '/^[a-z][0-9_a-z]{2,' . strval(self::ACTION_CODE_LENGTH - 1) . '}$/i';
//
//        if (preg_match($pattern, $string)) {
//            return true;
//        }
//        return false;
//    }

}
