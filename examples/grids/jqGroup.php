<?php

class jqGroup extends jqGrid
{

    const GROUP_TBL       = 'GROUP_TBL';
    const ID              = 'ID';
    const NAME            = 'NAME';
    const FORM_EDIT_WIDTH = 400;
    const GRID_WIDTH      = 600;

    protected function init()
    {
        $this->table = self::GROUP_TBL;

        $this->options = array(
            'caption' => 'Список абонентов',
            'width'   => self::GRID_WIDTH,
        );

        $this->query = "
            SELECT {fields}
            FROM {$this->table} 
            WHERE {where}
        ";


        # Set columns
        $this->cols = array(
            self::ID   => array(
                'label' => 'код группы',
                'width' => 40,
                'align' => 'center',
            ),
            self::NAME => array(
                'label' => 'код абонента',
                'width' => 100,
                'align' => 'center',
                'editable'=>true,
            ),
        );

        # Set nav
        $this->nav = array(
            'add'     => true,
            'edit'    => true,
            'del'     => true,
            'prmEdit' => array(
                'width' => self::FORM_EDIT_WIDTH,
            ),
            'prmAdd'  => array(
                'width' => self::FORM_EDIT_WIDTH,
            ),
        );

        # Add filter toolbar
        $this->render_filter_toolbar = true;

        $this->_scriptBefore[] = <<<JS

    var opts = {
        
        ondblClickRow: function(id)
        {
            $(this).editGridRow(id, { "width": "{$this->nav['prmEdit']['width']}" });
        },

    };
JS;
    }
}
