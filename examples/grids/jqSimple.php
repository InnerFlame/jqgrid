<?php

class jqSimple extends jqGrid {

    protected function init() {
        #Set database table
        $this->table = 'user';

        $this->query = "
            SELECT {fields}
            FROM {$this->table} i
            WHERE {where}
        ";

        #Make all columns editable by default
        $this->cols_default = array('editable' => true);

        #Set columns
        $this->cols = array(
            'id' => array(
                'label' => 'ID',
                'width' => 10,
                'align' => 'center',
                'editable' => false, //id is non-editable
            ),
            'name' => array(
                'db' => "CONCAT(i.name, ' ', i.s_name)",
                'label' => 'name',
                'width' => 35,
                'editrules' => array(
                    'required' => true
                ),
            ),
            'town' => array(
                'label' => 'town',
                'width' => 35,
                'editrules' => array(
                    'required' => true
                ),
            ),
            'price' => array(
                'label' => 'price',
                'width' => 35,
                'editrules' => array(
                    'required' => true
                ),
                'formatter' => 'integer',
            ),
            'diff_price' => array(
                'label' => 'Diff',
                'manual' => true,
                'width' => 12,
                'search' => false,
                'sortable' => false,
                'align' => 'right',
            ),
        );

        #Set nav
        $this->nav = array('add' => true, 'edit' => true, 'del' => true, 'excel' => true, 'edittext' => 'Edit', 'deltext' => 'Delete', 'viewtext' => 'View',);


        #Add filter toolbar
        $this->render_filter_toolbar = true;
    }

    protected function parseRow($r) {
        #Calc diff_price in PHP
        $r['diff_price'] = $r['price'] + 1;

        #Highlight customers with discount > 0.1
        $r['_class'] = array('name' => ($r['diff_price'] > 0) ? 'bold font-green' : null);

        return $r;
    }

}
